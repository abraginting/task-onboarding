package com.android.motion.view.adapter;

import com.android.motion.R;
import com.android.motion.model.VideoModel;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoTrailerAdapter, v 0.1 25/04/19 16.35 by Abraham Ginting
 */
public class VideoTrailerAdapter extends RecyclerView.Adapter<VideoTrailerAdapter.VideoTrailerViewHolder> {

    private final LayoutInflater layoutInflater;

    private Context context;

    private List<VideoModel> videoModels;

    private OnItemClickListener onItemClickListener;

    @Inject
    public VideoTrailerAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.videoModels = Collections.emptyList();
    }

    @NonNull
    @Override

    public VideoTrailerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = this.layoutInflater
            .inflate(R.layout.list_item_video_trailer, parent, false);
        return new VideoTrailerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoTrailerViewHolder holder, int position) {
        final VideoModel videoModel = videoModels.get(position);
        String youtubeBuiltVideoUrl = isSiteYoutube(videoModel.getSite());

        if (youtubeBuiltVideoUrl != null) {
            StringBuilder urlBuilt = new StringBuilder().append(youtubeBuiltVideoUrl)
                .append(videoModel.getKey());
            holder.tvVideoTrailer.setText(urlBuilt);
        }

        holder.tvVideoTrailer.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onVideoUrlItemClicked(videoModel);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return videoModels != null ? videoModels.size() : 0;
    }

    public void setVideoTrailerList(List<VideoModel> videoModels) {
        validateVideoTrailerList(videoModels);
        this.videoModels = videoModels;
        Log.e("TAG", "setVideoTrailerList: " + videoModels.size());
        notifyDataSetChanged();
    }

    public void validateVideoTrailerList(List<VideoModel> videoModels) {
        if (videoModels == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    private String isSiteYoutube(String site) {
        String urlBuilt = "";
        if (site.equalsIgnoreCase("youtube")) {
            urlBuilt = "https://www.youtube.com/watch?v=";
        } else {
            return null;
        }
        return urlBuilt;
    }

    public void setOnItemClickListener(
        OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onVideoUrlItemClicked(VideoModel videoModel);
    }

    public class VideoTrailerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_video_trailer_url)
        TextView tvVideoTrailer;

        public VideoTrailerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
