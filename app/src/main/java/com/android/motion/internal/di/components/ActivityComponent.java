package com.android.motion.internal.di.components;

import com.android.motion.internal.di.PerActivity;
import com.android.motion.internal.di.module.ActivityModule;

import android.app.Activity;

import dagger.Component;

/**
 * A base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 */
@PerActivity
@Component(
    dependencies = ApplicationComponent.class,
    modules = ActivityModule.class
)
interface ActivityComponent {

    //Exposed to sub-graphs.
    Activity activity();
}
