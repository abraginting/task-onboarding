package com.android.motion.internal.di.components;

import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.FavoriteMovieRepository;
import com.android.domain.repository.MovieRepository;
import com.android.domain.repository.VideoTrailerRepository;
import com.android.motion.internal.di.module.ApplicationModule;
import com.android.motion.view.activity.BaseActivity;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application
 */
@Singleton // Constraints this component to one-per-application or unscope bindings.
@Component(
    modules = ApplicationModule.class
)
public interface ApplicationComponent {

    void inject(BaseActivity baseActivity);

    //Exposed to sub-graphs.
    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    MovieRepository movieRepository();

    FavoriteMovieRepository favoriteMovieRepository();
}
