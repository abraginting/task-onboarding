package com.android.data.videotrailer.repository.source.network;

import com.android.data.network.RestApi;
import com.android.data.videotrailer.VideoTrailerEntityData;
import com.android.data.videotrailer.repository.source.network.result.VideoTrailerResult;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version NetworkVideoTrailerEntityData, v 0.1 25/04/19 14.04 by Abraham Ginting
 */
public class NetworkVideoTrailerEntityData implements VideoTrailerEntityData {

    private final RestApi restApi;

    @Inject
    public NetworkVideoTrailerEntityData(
        RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<VideoTrailerResult> getAllVideoById(int movieId) {
        return restApi.getVideoTrailerById(movieId);
    }
}
