package com.android.domain.interactor;

import com.android.domain.FavoriteMovie;
import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.FavoriteMovieRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version GetFavoriteMovieList.java, v 0.1 15/04/19 12.08 by Abraham Ginting
 */
public class GetFavoriteMovieList extends UseCase<List<FavoriteMovie>, Void> {

    private FavoriteMovieRepository repository;

    @Inject
    GetFavoriteMovieList(FavoriteMovieRepository favoriteMovieRepository,
        ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = favoriteMovieRepository;
    }

    @Override
    Observable<List<FavoriteMovie>> buildUseCaseObservable(Void aVoid) {
        return this.repository.getFavoriteMovie();
    }
}
