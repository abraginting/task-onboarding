package com.android.data.videotrailer.repository;

import com.android.data.videotrailer.VideoTrailerEntityData;
import com.android.data.videotrailer.mapper.VideoTrailerMapper;
import com.android.data.videotrailer.repository.source.VideoTrailerDataFactory;
import com.android.domain.Video;
import com.android.domain.repository.VideoTrailerRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoTrailerEntityRepository, v 0.1 25/04/19 12.58 by Abraham Ginting
 */
@Singleton
public class VideoTrailerEntityRepository implements VideoTrailerRepository {

    private final VideoTrailerDataFactory entityDataFactory;

    private final VideoTrailerMapper entityDataMapper;

    @Inject
    public VideoTrailerEntityRepository(
        VideoTrailerDataFactory entityDataFactory,
        VideoTrailerMapper entityDataMapper) {
        this.entityDataFactory = entityDataFactory;
        this.entityDataMapper = entityDataMapper;
    }

    @Override
    public Observable<List<Video>> getVideoTrailers(int movieId) {
        return createDataStore().getAllVideoById(movieId).map(entityDataMapper::map);
    }

    private VideoTrailerEntityData createDataStore() {
        return entityDataFactory.createData("Network");
    }
}
