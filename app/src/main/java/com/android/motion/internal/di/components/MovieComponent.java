package com.android.motion.internal.di.components;

import com.android.motion.internal.di.PerActivity;
import com.android.motion.internal.di.module.ActivityModule;
import com.android.motion.internal.di.module.MovieModule;
import com.android.motion.view.fragment.DetailsMovieFragment;
import com.android.motion.view.fragment.FavoriteMovieFragment;
import com.android.motion.view.fragment.PopularMovieFragment;
import com.android.motion.view.fragment.TopRatedMovieFragment;

import dagger.Component;

/**
 * Injects movie specific Fragments.
 */
@PerActivity
@Component(
    dependencies = ApplicationComponent.class,
    modules = {
        ActivityModule.class,
        MovieModule.class
    }
)
public interface MovieComponent extends ActivityComponent {

    void inject(PopularMovieFragment popularMovieFragment);

    void inject(TopRatedMovieFragment topRatedMovieFragment);

    void inject(DetailsMovieFragment detailsMovieFragment);

    void inject(FavoriteMovieFragment favoriteMovieFragment);
}
