package com.android.motion.view;

import com.android.motion.model.MovieModel;
import com.android.motion.model.VideoModel;

import java.util.List;

/**
 * Interface representing a View in a MVP pattern.
 * In this case is used as a view representing a movie details.
 */
public interface MovieDetailsView extends LoadDataView {

    void renderMovie(MovieModel movieModel);

    void onLikedMovie(boolean success);

    void onRemoveMovie(boolean remove);

    void isMovieFavorite();

    void showVideoTrailer(List<VideoModel> videoModels);

}
