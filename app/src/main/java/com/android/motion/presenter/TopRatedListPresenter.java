package com.android.motion.presenter;

import com.android.domain.Movie;
import com.android.domain.exception.DefaulErrorBundle;
import com.android.domain.exception.ErrorBundle;
import com.android.domain.interactor.DefaultObserver;
import com.android.domain.interactor.GetTopRatedMovieList;
import com.android.motion.exception.ErrorMessageFactory;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.mapper.MovieModelDataMapper;
import com.android.motion.model.MovieModel;
import com.android.motion.view.MovieListView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class TopRatedListPresenter implements Presenter {

    private static final String TAG = TopRatedListPresenter.class.getSimpleName();

    private GetTopRatedMovieList getTopRatedMovieListUseCase;

    private MovieListView movieListView;

    private MovieModelDataMapper movieModelDataMapper;

    @Inject
    public TopRatedListPresenter(GetTopRatedMovieList getTopRatedMovieList,
        MovieModelDataMapper movieModelDataMapper) {
        this.getTopRatedMovieListUseCase = getTopRatedMovieList;
        this.movieModelDataMapper = movieModelDataMapper;
    }

    public void setView(MovieListView movieListView) {
        this.movieListView = movieListView;
    }

    @Override
    public void resume() {
        //No implementation
    }

    @Override
    public void pause() {
        //No implementation
    }

    @Override
    public void destroy() {
        getTopRatedMovieListUseCase.dispose();
        movieListView = null;
    }

    public void initialize() {
        this.loadTopRatedMovieList();
    }

    private void loadTopRatedMovieList() {
        getTopRateMovieList();
    }

    private void showViewLoading() {
        movieListView.showLoading();
    }

    private void getTopRateMovieList() {
        showViewLoading();
        getTopRatedMovieListUseCase.execute(new DefaultObserver<List<Movie>>() {
            @Override
            public void onNext(List<Movie> movies) {
                Collection<MovieModel> movieModels = movieModelDataMapper.transform(movies);
                movieListView.renderMovieList(movieModels);
            }

            @Override
            public void onError(Throwable e) {
                hideViewLoading();
                showErrorMessage(new DefaulErrorBundle((Exception) e));
            }

            @Override
            public void onComplete() {
                hideViewLoading();
            }
        }, null);
    }

    public void onMovieClicked(MovieModel movieModel) {
        movieListView.viewMovie(movieModel);
    }

    private void hideViewLoading() {
        movieListView.hideLoading();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String message = ErrorMessageFactory
            .createErrorMessage(movieListView.context(), errorBundle.getException());
        movieListView.showError(message);
    }
}
