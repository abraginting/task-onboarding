package com.android.motion.presenter;

import com.android.domain.Movie;
import com.android.domain.exception.DefaulErrorBundle;
import com.android.domain.exception.ErrorBundle;
import com.android.domain.interactor.DefaultObserver;
import com.android.domain.interactor.GetPopularMovieList;
import com.android.motion.exception.ErrorMessageFactory;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.mapper.MovieModelDataMapper;
import com.android.motion.model.MovieModel;
import com.android.motion.view.MovieListView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

/**
 * {@link Presenter} that controls communication between views and models of the presentation layer.
 */
@PerActivity
public class PopularMovieListPresenter implements Presenter {

    private static final String TAG = PopularMovieListPresenter.class.getName();

    private GetPopularMovieList getPopularMovieListUseCase;

    private MovieListView movieListView;

    private MovieModelDataMapper movieModelDataMapper;

    @Inject
    public PopularMovieListPresenter(GetPopularMovieList getPopularMovieListUseCase,
        MovieModelDataMapper movieModelDataMapper) {
        this.getPopularMovieListUseCase = getPopularMovieListUseCase;
        this.movieModelDataMapper = movieModelDataMapper;
    }

    public void setView(@NonNull MovieListView view) {
        this.movieListView = view;
    }

    @Override
    public void resume() {
        //No Implementation
    }

    @Override
    public void pause() {
        //No implementation
    }

    @Override
    public void destroy() {
        this.getPopularMovieListUseCase.dispose();
        this.movieListView = null;
    }

    /**
     * Initializes the presenter by start retrieving the movie list/
     */
    public void initialize() {
        this.loadMovieList();
    }

    /**
     * Loads all movies.
     */
    private void loadMovieList() {
        this.getPopularMovieList();
    }

    private void showViewLoading() {
        this.movieListView.showLoading();
    }

    private void getPopularMovieList() {
        this.showViewLoading();
        this.getPopularMovieListUseCase.execute(new DefaultObserver<List<Movie>>() {
            @Override
            public void onNext(List<Movie> movies) {
                Collection<MovieModel> movieModels = movieModelDataMapper.transform(movies);
                movieListView.renderMovieList(movieModels);
            }

            @Override
            public void onError(Throwable e) {
                hideViewLoading();
                showErrorMessage(new DefaulErrorBundle((Exception) e));
            }

            @Override
            public void onComplete() {
                hideViewLoading();
            }
        }, null);
    }

    private void hideViewLoading() {
        this.movieListView.hideLoading();
    }

    public void onMovieClicked(MovieModel movieModel) {
        this.movieListView.viewMovie(movieModel);
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory
            .createErrorMessage(this.movieListView.context(), errorBundle.getException());
        this.movieListView.showError(errorMessage);
    }
}
