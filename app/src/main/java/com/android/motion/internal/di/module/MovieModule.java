package com.android.motion.internal.di.module;

import dagger.Module;

/**
 * Dagger module that provides movie related collaborators
 */
@Module
public class MovieModule {

    public MovieModule() {
    }
}
