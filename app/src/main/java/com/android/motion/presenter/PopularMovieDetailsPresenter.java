package com.android.motion.presenter;

import com.android.domain.FavoriteMovie;
import com.android.domain.Movie;
import com.android.domain.Video;
import com.android.domain.exception.DefaulErrorBundle;
import com.android.domain.exception.ErrorBundle;
import com.android.domain.interactor.AddFavoriteMovie;
import com.android.domain.interactor.CheckMovieIsFavorite;
import com.android.domain.interactor.DefaultObserver;
import com.android.domain.interactor.GetMovieDetails;
import com.android.domain.interactor.GetVideoTrailerById;
import com.android.domain.interactor.RemoveMovieIsFavorite;
import com.android.motion.exception.ErrorMessageFactory;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.mapper.MovieModelDataMapper;
import com.android.motion.mapper.VideoModelDataMapper;
import com.android.motion.model.FavoriteMovieModel;
import com.android.motion.model.MovieModel;
import com.android.motion.model.VideoModel;
import com.android.motion.view.MovieDetailsView;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

/**
 * Presenter that controls communication between views and models of the presentation layer.
 */
@PerActivity
public class PopularMovieDetailsPresenter implements Presenter {

    private static final String TAG = PopularMovieDetailsPresenter.class.getSimpleName();

    private GetMovieDetails getMovieDetailsUseCase;

    private CheckMovieIsFavorite checkMovieIsFavoriteUseCase;

    private RemoveMovieIsFavorite removeMovieIsFavoriteUseCase;

    private AddFavoriteMovie addFavoriteMovieUseCase;

    private GetVideoTrailerById getVideoTrailerByIdUseCase;

    private MovieDetailsView movieDetailsView;

    private MovieModelDataMapper movieModelDataMapper;

    private VideoModelDataMapper videoModelDataMapper;

    @Inject
    public PopularMovieDetailsPresenter(GetMovieDetails getMovieDetailsUseCase,
        AddFavoriteMovie addFavoriteMovie, CheckMovieIsFavorite checkMovieIsFavoriteUseCase,
        RemoveMovieIsFavorite removeMovieIsFavoriteUseCase,
        MovieModelDataMapper movieModelDataMapper,
        VideoModelDataMapper videoModelDataMapper, GetVideoTrailerById getVideoTrailerByIdUseCase) {
        this.getMovieDetailsUseCase = getMovieDetailsUseCase;
        this.addFavoriteMovieUseCase = addFavoriteMovie;
        this.movieModelDataMapper = movieModelDataMapper;
        this.checkMovieIsFavoriteUseCase = checkMovieIsFavoriteUseCase;
        this.removeMovieIsFavoriteUseCase = removeMovieIsFavoriteUseCase;
        this.videoModelDataMapper = videoModelDataMapper;
        this.getVideoTrailerByIdUseCase = getVideoTrailerByIdUseCase;
    }

    public void setView(@NonNull MovieDetailsView view) {
        this.movieDetailsView = view;
    }

    @Override
    public void resume() {
        //No implementation
    }

    @Override
    public void pause() {
        //No implementation
    }

    @Override
    public void destroy() {
        this.getMovieDetailsUseCase.dispose();
        this.addFavoriteMovieUseCase.dispose();
        this.getVideoTrailerByIdUseCase.dispose();
        this.movieDetailsView = null;
    }

    /**
     * Initializes the presenter by showing/hiding proper views and retrieving movie details.
     */
    public void initialize(String movieId) {
        this.hideViewRetry();
        this.showViewLoading();
        if (movieId != null) {
            this.getMovieDetails(movieId);
            //this.getVideoTrailer(movieId);
            this.checkMovieIsFavorite(movieId);
        }
    }

    private void hideViewRetry() {
        this.movieDetailsView.hideRetry();
    }

    private void showViewLoading() {
        this.movieDetailsView.showLoading();
    }

    private void getMovieDetails(String movieId) {
        this.getMovieDetailsUseCase
            .execute(new DefaultObserver<Movie>() {
                @Override
                public void onNext(Movie movie) {
                    MovieModel movieModel = movieModelDataMapper.transform(movie);
                    movieDetailsView.renderMovie(movieModel);
                    hideViewLoading();
                }

                @Override
                public void onError(Throwable e) {
                    hideViewLoading();
                    showErrorMessage(new DefaulErrorBundle((Exception) e));
                }
            }, GetMovieDetails.Params.forMovie(movieId));
    }

    private void hideViewLoading() {
        this.movieDetailsView.hideLoading();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory
            .createErrorMessage(this.movieDetailsView.context(), errorBundle.getException());
        this.movieDetailsView.showError(errorMessage);
    }

    public void addMovieFavorite(FavoriteMovieModel favoriteMovieModel) {
        addFavoriteMovieUseCase.execute(new DefaultObserver<Boolean>() {

            @Override
            public void onNext(Boolean success) {
                movieDetailsView.onLikedMovie(success);
            }

            @Override
            public void onError(Throwable e) {
                hideViewLoading();
                showErrorMessage(new DefaulErrorBundle((Exception) e));
                movieDetailsView.onLikedMovie(false);
            }

        }, AddFavoriteMovie.Params.forAddFavoriteMovie(favoriteMovieModel.getMovieId(),
            favoriteMovieModel.getVoteAverage(), favoriteMovieModel.getPosterPath(),
            favoriteMovieModel.getOriginalTitle(), favoriteMovieModel.isFavorite()));
    }

    private void checkMovieIsFavorite(String movieId) {
        checkMovieIsFavoriteUseCase.execute(new DefaultObserver<FavoriteMovie>() {

            @Override
            public void onComplete() {
                movieDetailsView.isMovieFavorite();
            }
        }, CheckMovieIsFavorite.Params.forCheckMovieIsFavorite(movieId));
    }

    public void removeMovieFavorite(String movieId) {
        removeMovieIsFavoriteUseCase.execute(new DefaultObserver<Boolean>() {
            @Override
            public void onNext(Boolean remove) {
                movieDetailsView.onRemoveMovie(remove);
            }

            @Override
            public void onError(Throwable e) {
                showErrorMessage(new DefaulErrorBundle((Exception) e));
            }
        }, RemoveMovieIsFavorite.Params.forRemoveMovieIsFavorite(movieId));
    }

    private void getVideoTrailer(String movieId) {
        getVideoTrailerByIdUseCase.execute(new DefaultObserver<List<Video>>() {
            @Override
            public void onNext(List<Video> videoList) {
                Log.e(TAG, "onNext: " + videoList.size());
                List<VideoModel> videoModels = videoModelDataMapper.mapper(videoList);
                Log.e(TAG, "onNext: " + videoModels.size());
                movieDetailsView.showVideoTrailer(videoModels);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
                showErrorMessage(new DefaulErrorBundle((Exception) e));
            }
        }, GetVideoTrailerById.Params.forVideoTrailer(Integer.parseInt(movieId)));
    }

    public void onVideoTrailerClicker(VideoModel videoModel) {
        //Todo open youtube app
    }
}
