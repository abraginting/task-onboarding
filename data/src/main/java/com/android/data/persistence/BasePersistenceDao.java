package com.android.data.persistence;

import com.android.data.favoritemovie.repository.source.persistence.dao.FavoriteMovieDao;
import com.android.data.favoritemovie.repository.source.persistence.entity.FavoriteMovieEntity;

import androidx.room.Database;
import androidx.room.RoomDatabase;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version BasePersistenceDao.java, v 0.1 12/04/19 16.28 by Abraham Ginting
 */
@Database(entities = {FavoriteMovieEntity.class}, version = 1, exportSchema = false)
public abstract class BasePersistenceDao extends RoomDatabase {

    public abstract FavoriteMovieDao favoriteMovieDao();
}
