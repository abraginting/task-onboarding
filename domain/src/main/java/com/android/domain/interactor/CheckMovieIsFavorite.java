package com.android.domain.interactor;

import com.android.domain.FavoriteMovie;
import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.FavoriteMovieRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version CheckMovieIsFavorite, v 0.1 23/04/19 15.56 by Abraham Ginting
 */
public class CheckMovieIsFavorite extends UseCase<FavoriteMovie, CheckMovieIsFavorite.Params> {

    private FavoriteMovieRepository repository;

    @Inject
    CheckMovieIsFavorite(ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread, FavoriteMovieRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    Observable<FavoriteMovie> buildUseCaseObservable(Params params) {
        return repository.checkMovieIsFavorite(params.movieId);
    }

    public static class Params {

        private String movieId;

        Params(String movieId) {
            this.movieId = movieId;
        }

        public static Params forCheckMovieIsFavorite(String movieId) {
            return new CheckMovieIsFavorite.Params(movieId);
        }
    }
}
