package com.android.motion.mapper;

import com.android.domain.Video;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.model.VideoModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoModelDataMapper, v 0.1 25/04/19 10.52 by Abraham Ginting
 */
@PerActivity
public class VideoModelDataMapper {

    @Inject
    public VideoModelDataMapper() {
    }

    public List<VideoModel> mapper(List<Video> videos) {
        List<VideoModel> videoModels = new ArrayList<>();

        for (int i = 0; i < videos.size(); i++) {
            VideoModel videoModel = new VideoModel(videos.get(i).getVideoId());
            videoModel.setKey(videos.get(i).getKey());
            videoModel.setName(videos.get(i).getName());
            videoModel.setSite(videos.get(i).getSite());
            videoModels.add(videoModel);
        }

        return videoModels;
    }
}
