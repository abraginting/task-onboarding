package com.android.motion;

import com.android.motion.internal.di.components.ApplicationComponent;
import com.android.motion.internal.di.components.DaggerApplicationComponent;
import com.android.motion.internal.di.module.ApplicationModule;
import com.squareup.leakcanary.LeakCanary;

import android.app.Application;
import android.os.SystemClock;

public class App extends Application {

    private static final int sleepLimit = 2500;

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
        this.initializeLeakDetection();
        this.delaySplashActivity();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .build();
    }

    private void initializeLeakDetection() {
        if (BuildConfig.DEBUG) {
            LeakCanary.install(this);
        }
    }

    private void delaySplashActivity() {
        SystemClock.sleep(sleepLimit);
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
