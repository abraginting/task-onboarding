package com.android.domain.repository;

import com.android.domain.FavoriteMovie;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version FavoriteMovieRepository.java, v 0.1 12/04/19 17.26 by Abraham Ginting
 */
public interface FavoriteMovieRepository {

    Observable<List<FavoriteMovie>> getFavoriteMovie();

    Observable<Boolean> addFavoriteMovie(String movieId, String title, String posterPath,
        Double voteAverage, Boolean isFavorite);

    Observable<FavoriteMovie> checkMovieIsFavorite(String movieId);

    Observable<Boolean> removeFavoriteMovie(String movieId);
}
