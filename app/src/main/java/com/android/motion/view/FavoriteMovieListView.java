package com.android.motion.view;

import com.android.motion.model.FavoriteMovieModel;

import java.util.Collection;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version FavoriteMovieListView.java, v 0.1 15/04/19 13.10 by Abraham Ginting
 */
public interface FavoriteMovieListView extends LoadDataView {

    void renderFavoriteMovieList(Collection<FavoriteMovieModel> favoriteMovieModelCollection);

    void viewFavoriteMovie(FavoriteMovieModel favoriteMovieModel);

}
