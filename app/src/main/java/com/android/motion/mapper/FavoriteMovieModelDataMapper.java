package com.android.motion.mapper;

import com.android.domain.FavoriteMovie;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.model.FavoriteMovieModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version FavoriteMovieModelDataMapper.java, v 0.1 15/04/19 12.09 by Abraham Ginting
 */
@PerActivity
public class FavoriteMovieModelDataMapper {

    @Inject
    public FavoriteMovieModelDataMapper() {

    }

    public Collection<FavoriteMovieModel> transform(
        Collection<FavoriteMovie> favoriteMovieCollection) {
        Collection<FavoriteMovieModel> favoriteMovieModelCollection;

        if (favoriteMovieCollection != null && !favoriteMovieCollection.isEmpty()) {
            favoriteMovieModelCollection = new ArrayList<>();
            for (FavoriteMovie favoriteMovie : favoriteMovieCollection) {
                favoriteMovieModelCollection.add(transform(favoriteMovie));
            }
        } else {
            favoriteMovieModelCollection = Collections.emptyList();
        }

        return favoriteMovieModelCollection;
    }

    public FavoriteMovieModel transform(FavoriteMovie favoriteMovie) {
        if (favoriteMovie == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final FavoriteMovieModel favoriteMovieModel = new FavoriteMovieModel(
            favoriteMovie.getMovieId());
        favoriteMovieModel.setOriginalTitle(favoriteMovie.getOriginalTitle());
        favoriteMovieModel.setFavorite(favoriteMovie.getFavorite());
        favoriteMovieModel.setPosterPath(favoriteMovie.getPosterPath());
        favoriteMovieModel.setVoteAverage(favoriteMovie.getVoteAverage());
        favoriteMovieModel.setFavorite(favoriteMovie.getFavorite());
        return favoriteMovieModel;
    }
}

