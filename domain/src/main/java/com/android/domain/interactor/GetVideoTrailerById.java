package com.android.domain.interactor;

import com.android.domain.Video;
import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.MovieRepository;
import com.fernandocejas.arrow.checks.Preconditions;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version GetVideoTrailerById, v 0.1 25/04/19 10.36 by Abraham Ginting
 */
public class GetVideoTrailerById extends UseCase<List<Video>, GetVideoTrailerById.Params> {

    private MovieRepository repository;

    @Inject
    GetVideoTrailerById(MovieRepository repository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    Observable<List<Video>> buildUseCaseObservable(Params params) {
        Preconditions.checkNotNull(params);
        return repository.retrofitVideoTrailer(params.movieId);
    }

    public static final class Params {

        private final int movieId;

        private Params(int movieId) {
            this.movieId = movieId;
        }

        public static Params forVideoTrailer(int movieId) {
            return new Params(movieId);
        }

    }
}
