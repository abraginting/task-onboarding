package com.android.motion.internal.di.module;

import com.android.data.executor.JobExecutor;
import com.android.data.favoritemovie.repository.FavoriteMovieEntityRepository;
import com.android.data.repository.MovieEntityRepository;
import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.FavoriteMovieRepository;
import com.android.domain.repository.MovieRepository;
import com.android.motion.App;
import com.android.motion.UIThread;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides objects which will live during the application lifecycle
 */
@Module
public class ApplicationModule {

    private final App application;

    public ApplicationModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    MovieRepository provideMovieRepository(MovieEntityRepository movieEntityRepository) {
        return movieEntityRepository;
    }

    @Provides
    @Singleton
    FavoriteMovieRepository provideFavoriteMovieRepository(
        FavoriteMovieEntityRepository favoriteMovieEntityRepository) {
        return favoriteMovieEntityRepository;
    }
}
