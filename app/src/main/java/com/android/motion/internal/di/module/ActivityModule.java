package com.android.motion.internal.di.module;

import com.android.motion.internal.di.PerActivity;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * A module to wrap the Activity state and expose it to the graph
 */
@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependants in the graph
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
