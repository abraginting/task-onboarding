package com.android.motion.model;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoModel, v 0.1 25/04/19 10.51 by Abraham Ginting
 */
public class VideoModel {

    private String videoId;

    private String name;

    private String key;

    private String site;

    public VideoModel() {
    }

    public VideoModel(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
