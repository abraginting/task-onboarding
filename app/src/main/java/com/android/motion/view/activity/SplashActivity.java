package com.android.motion.view.activity;

import com.android.motion.R;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        redirectToMainActivity();
    }

    private void redirectToMainActivity() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }
}
