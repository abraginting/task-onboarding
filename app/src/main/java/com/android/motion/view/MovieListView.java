package com.android.motion.view;

import com.android.motion.model.MovieModel;

import java.util.Collection;

/**
 * Interface representing a View in a MVP pattern.
 * In this case is used as a view representing a list of MovieModel.
 */
public interface MovieListView extends LoadDataView {

    void renderMovieList(Collection<MovieModel> movieModelCollection);

    void viewMovie(MovieModel movieModel);
}
