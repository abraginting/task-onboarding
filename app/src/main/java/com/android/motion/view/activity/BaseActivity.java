package com.android.motion.view.activity;

import com.android.motion.App;
import com.android.motion.internal.di.components.ApplicationComponent;
import com.android.motion.internal.di.module.ActivityModule;
import com.android.motion.navigation.Navigator;

import android.os.Bundle;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager()
            .beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * Get the Main Application Component for Dependency Injection
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((App) getApplication()).getApplicationComponent();
    }

    /**
     * Get an Activity Module for Dependency Injection
     */
    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }
}
