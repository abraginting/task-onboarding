package com.android.domain.interactor;

import com.android.domain.executor.PostExecutionThread;
import com.android.domain.executor.ThreadExecutor;
import com.android.domain.repository.FavoriteMovieRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version RemoveMovieIsFavorite, v 0.1 23/04/19 17.32 by Abraham Ginting
 */
public class RemoveMovieIsFavorite extends UseCase<Boolean, RemoveMovieIsFavorite.Params> {

    private FavoriteMovieRepository repository;

    @Inject
    RemoveMovieIsFavorite(FavoriteMovieRepository repository, ThreadExecutor threadExecutor,
        PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    Observable<Boolean> buildUseCaseObservable(Params params) {
        return repository.removeFavoriteMovie(params.movieId);
    }

    public static class Params {

        private String movieId;

        Params(String movieId) {
            this.movieId = movieId;
        }

        public static Params forRemoveMovieIsFavorite(String movieId) {
            return new RemoveMovieIsFavorite.Params(movieId);
        }
    }
}
