package com.android.domain.repository;

import com.android.domain.Movie;
import com.android.domain.Video;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a Repository for getting {@link com.android.domain.Movie} related data.
 */
public interface MovieRepository {

    /**
     * Get an {@link Observable} which will emit a List of {@link com.android.domain.Movie}
     */
    Observable<List<Movie>> popularMovies();

    Observable<List<Movie>> topRatedMovies();

    /**
     * Get an {@link Observable} which will emit a {@link Movie}.
     *
     * @param movieId The movie id used to retrieve movie data.
     */
    Observable<Movie> movie(final String movieId);

    /**
     * Get an {@link Observable} which will emit a List of {@link com.android.domain.Movie} using
     * Retrofit
     */
    Observable<List<Movie>> retrofitPopularMovies();

    Observable<List<Movie>> retrofitTopRateMovies();

    Observable<Movie> retrofitDetailMovie(String movieId);

    Observable<List<Video>> retrofitVideoTrailer(int movieId);

}
