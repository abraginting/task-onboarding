package com.android.motion.exception;

import com.android.data.exception.MovieNotFoundException;
import com.android.data.exception.NetworkConnectionException;
import com.android.motion.R;

import android.content.Context;

public class ErrorMessageFactory {

    private ErrorMessageFactory() {
    }

    /**
     * Creates a String representing an error message
     */
    public static String createErrorMessage(Context context, Exception exception) {
        String message = context.getString(R.string.exception_message_generic);

        if (exception instanceof NetworkConnectionException) {
            message = context.getString(R.string.exception_message_no_connection);
        } else if (exception instanceof MovieNotFoundException) {
            message = context.getString(R.string.exception_message_movie_not_found);
        }

        return message;
    }
}
