package com.android.motion.mapper;

import com.android.domain.FavoriteMovie;
import com.android.domain.Movie;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.model.MovieModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Mapper class used to transform Movie in the domain layer to MovieModel in app layer.
 */
@PerActivity
public class MovieModelDataMapper {

    @Inject
    public MovieModelDataMapper() {
    }

    /**
     * Transform a Collection of Movie into a Collection of MovieModel
     */
     public Collection<MovieModel> transform(Collection<Movie> movieCollection) {
        Collection<MovieModel> movieModelCollection;

        if (movieCollection != null && !movieCollection.isEmpty()) {
            movieModelCollection = new ArrayList<>();
            for (Movie movie : movieCollection) {
                movieModelCollection.add(transform(movie));
            }
        } else {
            movieModelCollection = Collections.emptyList();
        }

        return movieModelCollection;
    }

    /**
     * Transform a Movie into an MovieModel.
     */
    public MovieModel transform(Movie movie) {
        if (movie == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final MovieModel movieModel = new MovieModel(movie.getMovieId());
        movieModel.setTitle(movie.getTitle());
        movieModel.setGenre(movie.getGenre());
        movieModel.setMovieRate(movie.getMovieRate());
        movieModel.setReleaseDate(movie.getReleaseDate());
        movieModel.setSynopsis(movie.getSynopsis());
        movieModel.setUrlPoster(movie.getUrlPoster());
        movieModel.setMovieDuration(movie.getMovieDuration());

        return movieModel;
    }
}
