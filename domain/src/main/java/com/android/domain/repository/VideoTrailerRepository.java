package com.android.domain.repository;

import com.android.domain.Video;

import java.util.List;

import io.reactivex.Observable;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoTrailerRepository, v 0.1 25/04/19 10.39 by Abraham Ginting
 */
public interface VideoTrailerRepository {

    Observable<List<Video>> getVideoTrailers(int movieId);
}
