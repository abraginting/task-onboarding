package com.android.data.repository.datasource;

import com.android.data.entity.MovieEntity;
import com.android.data.entity.MovieResponse;
import com.android.data.network.RestApi;
import com.android.domain.VideoResult;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link MovieEntityData} implementation based on connections to the API.
 */
public class NetworkMovieEntityData implements MovieEntityData {

    private final RestApi restApi;

    /**
     * Construct a {@link MovieEntityData} based on connection to the API.
     */
    NetworkMovieEntityData(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<List<MovieEntity>> popularMovieEntityList() {
        return null;
    }

    @Override
    public Observable<List<MovieEntity>> topRatedMovieEntityList() {
        return null;
    }

    @Override
    public Observable<MovieEntity> movieEntityDetails(String movieId) {
        return this.restApi.movieEntityById(movieId);
    }

    @Override
    public Observable<MovieResponse> retrofitPopularMovieEntityList() {
        return this.restApi.getPopularMovieRetrofit();
    }

    @Override
    public Observable<MovieResponse> retrofitTopRatedMovieEntityList() {
        return this.restApi.getTopRatedMovieRetrofit();
    }

    @Override
    public Observable<MovieEntity> retrofitDetailsMovie(String movieId) {
        return this.restApi.getMovieDetailsRetrofit(Integer.parseInt(movieId));
    }
}