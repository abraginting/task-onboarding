package com.android.data.videotrailer.repository.source;

import com.android.data.videotrailer.VideoTrailerEntityData;
import com.android.data.videotrailer.repository.source.mock.MockVideoTrailerEntityData;
import com.android.data.videotrailer.repository.source.network.NetworkVideoTrailerEntityData;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoTrailerDataFactory, v 0.1 25/04/19 14.02 by Abraham Ginting
 */
@Singleton
public class VideoTrailerDataFactory {

    private final MockVideoTrailerEntityData entityDataMock;

    private final NetworkVideoTrailerEntityData entityDataNetwork;

    @Inject
    public VideoTrailerDataFactory(
        MockVideoTrailerEntityData entityDataMock,
        NetworkVideoTrailerEntityData entityDataNetwork) {
        this.entityDataMock = entityDataMock;
        this.entityDataNetwork = entityDataNetwork;
    }

    public VideoTrailerEntityData createData(String source) {
        switch (source) {
            case "Mock":
                return entityDataMock;
            case "Network":
                return entityDataNetwork;
            default:
                return null;
        }
    }
}
