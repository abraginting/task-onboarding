package com.android.domain;

import java.util.List;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version VideoResult, v 0.1 24/04/19 15.12 by Abraham Ginting
 */
public class VideoResult {

    private List<Video> videos;

    public VideoResult() {
    }

    public VideoResult(List<Video> videos) {
        this.videos = videos;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }
}
