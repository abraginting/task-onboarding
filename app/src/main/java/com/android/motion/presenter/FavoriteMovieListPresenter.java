package com.android.motion.presenter;

import com.android.domain.FavoriteMovie;
import com.android.domain.exception.DefaulErrorBundle;
import com.android.domain.exception.ErrorBundle;
import com.android.domain.interactor.DefaultObserver;
import com.android.domain.interactor.GetFavoriteMovieList;
import com.android.motion.exception.ErrorMessageFactory;
import com.android.motion.internal.di.PerActivity;
import com.android.motion.mapper.FavoriteMovieModelDataMapper;
import com.android.motion.model.FavoriteMovieModel;
import com.android.motion.view.FavoriteMovieListView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;

/**
 * @author Abraham Ginting (abraham.ginting@dana.id)
 * @version FavoriteMovieListPresenter.java, v 0.1 15/04/19 12.06 by Abraham Ginting
 */
@PerActivity
public class FavoriteMovieListPresenter implements Presenter {

    private static final String TAG = PopularMovieListPresenter.class.getName();

    private FavoriteMovieListView favoriteMovieListView;

    private FavoriteMovieModelDataMapper favoriteMovieModelDataMapper;

    private GetFavoriteMovieList getFavoriteMovieListUseCase;

    @Inject
    public FavoriteMovieListPresenter(
        FavoriteMovieModelDataMapper favoriteMovieModelDataMapper,
        GetFavoriteMovieList getFavoriteMovieList) {
        this.getFavoriteMovieListUseCase = getFavoriteMovieList;
        this.favoriteMovieModelDataMapper = favoriteMovieModelDataMapper;

    }

    public void setView(@NonNull FavoriteMovieListView view) {
        this.favoriteMovieListView = view;
    }

    @Override
    public void resume() {
        //No implementation
    }

    @Override
    public void pause() {
        //No implementation
    }

    @Override
    public void destroy() {
        this.getFavoriteMovieListUseCase.dispose();
        this.favoriteMovieListView = null;
    }

    public void initialize() {
        this.loadMovieList();
    }

    private void loadMovieList() {
        this.showViewLoading();
        this.getFavoriteMovieList();
    }

    private void showViewLoading() {
        this.favoriteMovieListView.showLoading();
    }

    private void getFavoriteMovieList() {
        this.getFavoriteMovieListUseCase.execute(new DefaultObserver<List<FavoriteMovie>>() {
            @Override
            public void onNext(List<FavoriteMovie> favoriteMovies) {
                Collection<FavoriteMovieModel> favoriteMovieModels = favoriteMovieModelDataMapper
                    .transform(favoriteMovies);
                favoriteMovieListView.renderFavoriteMovieList(favoriteMovieModels);
                hideViewLoading();
            }

            @Override
            public void onError(Throwable e) {
                hideViewLoading();
                showErrorMessage(new DefaulErrorBundle((Exception) e));
            }
        }, null);
    }

    private void hideViewLoading() {
        this.favoriteMovieListView.hideLoading();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory
            .createErrorMessage(this.favoriteMovieListView.context(), errorBundle.getException());
        this.favoriteMovieListView.showError(errorMessage);
    }

    public void onMovieClicked(FavoriteMovieModel favoriteMovieModel) {
        this.favoriteMovieListView.viewFavoriteMovie(favoriteMovieModel);
    }

}
